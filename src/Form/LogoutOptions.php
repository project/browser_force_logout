<?php

namespace Drupal\browser_force_logout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;


/**
 * Provides configurable option to set logout options.
 *
 * @package Drupal\browser_force_logout\Form
 */
class LogoutOptions extends ConfigFormBase {


  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'browser_force_logout.role_options';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'browser_force_logout_role_options';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config(static::SETTINGS);
    $roles = Role::loadMultiple();
    $roleNames = array_keys($roles);

    $options = [];
    foreach ($roleNames as $roleName) {
      $options[$roleName] = $roleName;
    }

    $form['enable_force_logout'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Browser Force Logout'),
      '#default_value' => $config->get('enable_force_logout'),
    ];

    $form['role_options'] = [
      '#type' => 'select',
      '#title' => $this->t('Exclude Role'),
      '#description' => $this->t("Select the roles that you want to exclude"),
      '#options' => $options,
      '#multiple' => TRUE,
      '#default_value' => $config->get('role_options'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('enable_force_logout', $form_state->getValue('enable_force_logout'))
      ->set('role_options', $form_state->getValue('role_options'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
