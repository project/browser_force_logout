Browser Force Logout

Force Logout When Browser Closed is a powerful security module for Drupal that ensures user sessions are securely terminated when the browser is closed. With this module, you can safeguard user accounts by automatically logging them out, preventing unauthorized access in case the browser is accidentally left open or if the user forgets to log out.

The module provides an extra layer of protection to prevent session hijacking and unauthorized access to sensitive information. It actively monitors the browser session and immediately terminates the user's session when the browser window or tab is closed, offering peace of mind for site administrators and users alike.
