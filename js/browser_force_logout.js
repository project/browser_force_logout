(function($, Drupal, drupalSettings){
  'use strict';
  Drupal.behaviors.browserForceLogout = {
    attach: function (context) {
      $(window).on('beforeunload', function () {

        // Send an AJAX request to Drupal's logout URL.
        // Make it synchronous to ensure logout before the window closes.
        $.ajax({
          url: '/user/logout',
          method: 'POST',
          async: false,
          xhrFields: {
            withCredentials: true // Include cookies in the request.
          }
        });
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
